# Welcome to GNOME design!

The GNOME Design Team is responsible for GNOME user experience and visual design. The following links provide the main contact points and resources for GNOME design work.

If you want to contribute to GNOME design or become a member of the design team, please see [the onboarding guide](https://welcome.gnome.org/team/design/).

## Get in Touch

The main communication channel for GNOME design is [#gnome-design on Matrix](https://matrix.to/#/#gnome-design:gnome.org).

## Guidelines and Articles

* [GNOME Human Interface Guidelines](https://developer.gnome.org/hig/): GNOME's official design guidelines - required reading for all UX contributors!
* [Recommended articles and books](https://hedgedoc.gnome.org/design-reading?view): a list maintained by the design team

## UX Design Repositories

Repositories where mockups are stored:

* [app-mockups](https://gitlab.gnome.org/Teams/Design/app-mockups): core application designs
* [os-mockups](https://gitlab.gnome.org/Teams/Design/os-mockups): system and app platform/toolkit designs
* [settings-mockups](https://gitlab.gnome.org/Teams/Design/settings-mockups): designs for the Settings app
* [software-mockups](https://gitlab.gnome.org/Teams/Design/software-mockups): designs for the Software app

Each repository includes SVG sources which are typically edited in Inkscape.

## Icon and Wallpaper Design

Resources, guidelines and chat for visual design work:

* [GNOME wallpapers repository](https://gitlab.gnome.org/GNOME/gnome-backgrounds/)
* [Icon set repository](https://gitlab.gnome.org/Teams/Design/icon-development-kit)
* [App icon design channel](https://matrix.to/#/#appicondesign:gnome.org)
* [App icon design guidelines](https://developer.gnome.org/hig/guidelines/app-icons.html)

## Other Resources

See the HIG [tools and resources page](https://developer.gnome.org/hig/resources.html) for a list of helpful apps and templates.

## Team Members

Core members of the design team include:

* [Allan Day](https://gitlab.gnome.org/aday)
* [Tobias Bernard](https://gitlab.gnome.org/bertob)
* [Cassidy James Blaede](https://gitlab.gnome.org/cassidyjames)
* [Sam Hewitt](https://gitlab.gnome.org/snwh)
* [Jakub Steiner](https://gitlab.gnome.org/jimmac)
